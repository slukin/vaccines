# VACCINES

My notes relaed to vaccination.

## Vaccinated vs unvaccinated children

Vaccinations have prevented millions of infectious illnesses, hospitalizations and deaths among U.S. children, yet the long-term health outcomes of the vaccination schedule remain uncertain. Studies have been recommended by the U.S. Institute of Medicine to address this question.

...

Results to be published and presented in 2017

[https://www.researchgate.net/project/evaluation-of-childhood-vaccination-program](https://www.researchgate.net/project/evaluation-of-childhood-vaccination-program)

### Pilot comparative study on the health of vaccinated and unvaccinated 6-to 12-year-old U.S. children

This study aimed 1) to compare vaccinated and unvaccinated children on a broad range of health utcomes, and 2) to determine whether an association found between vaccination and eurodevelopmental disorders (NDD)

This study aimed 1) to compare vaccinated and unvaccinated children on a broad range of health outcomes, and 2) to determine whether an association found between vaccination and **neurodevelopmental disorders** (NDD)

> Neurodevelopmental disorders[[1\]](https://en.wikipedia.org/wiki/Neurodevelopmental_disorder#cite_note-Reynolds1-1) are impairments of the growth and development of the brain or [central nervous system](https://en.wikipedia.org/wiki/Central_nervous_system). A narrower use of the term refers to a disorder of [brain function](https://en.wikipedia.org/wiki/Brain_function) that affects [emotion](https://en.wikipedia.org/wiki/Emotion), [learning ability](https://en.wikipedia.org/wiki/General_Learning_Ability), [self-control](https://en.wikipedia.org/wiki/Self-control) and [memory](https://en.wikipedia.org/wiki/Memory) and that unfolds as an individual develops and grows.
>
> [https://en.wikipedia.org/wiki/Neurodevelopmental_disorder](https://en.wikipedia.org/wiki/Neurodevelopmental_disorder)

...

With regard to acute and chronic conditions, vaccinated children were significantly **less likely than the unvaccinated to have had chickenpox and pertussis but**, contrary to expectation, **were significantly more likely to have been diagnosed with otitis media, pneumonia, allergic rhinitis, eczema, and NDD**. The vaccinated were also **more likely to have used antibiotics, allergy and fever medications**; to have been fitted with ventilation ear tubes; **visited a doctor for a health issue in the previous year, and been hospitalized**. The reason for hospitalization and the age of the child at the time were not determined, but the latter finding appears consistent with a study of 38,801 reports to the VAERS of **infants** who **were hospitalized or had died after receiving vaccinations**. The study reported a **linear relationship between the number of vaccine doses administered at one time and the rate of hospitalization and death**; moreover, the **younger the infant at the time of vaccination, the higher was the rate of hospitalization and death** [55]. The hospitalization rate increased from 11% for 2 vaccine doses to 23.5% for 8 doses (r2 = 0.91), while the case fatality rate increased significantly from 3.6% for those receiving from 1-4 doses to 5.4 % for those receiving from 5-8 doses.

[https://www.researchgate.net/publication/317086531_Pilot_comparative_study_on_the_health_of_vaccinated_and_unvaccinated_6-to_12-year-old_US_children](https://www.researchgate.net/publication/317086531_Pilot_comparative_study_on_the_health_of_vaccinated_and_unvaccinated_6-to_12-year-old_US_children)

Главные мысли из этой цитаты о том, что показало исследование:

1) Вакцинированные дети were significantly more likely to have been diagnosed with otitis media, pneumonia, allergic rhinitis, eczema, and **NDD (Neurodevelopmental disorders)**. Vaccinated were more likely to have used antibiotics, allergy and fever medications ... visited a doctor for a health issue in the previous year, and been hospitalized.
*Из википедии: Neurodevelopmental disorders[1] are impairments of the growth and development of the brain or central nervous system. A narrower use of the term refers to a disorder of brain function that affects emotion, learning ability, self-control and memory and that unfolds as an individual develops and grows.*

2) **В 38 801 случаях** после вакцинирования дети **попали в больницу или умерли**: infants were hospitalized or had died after receiving vaccinations (это не все случаи, а из этого исследования)

3) Чем меньше **возраст** вакцинируемых, тем больше риск попасть в больницу и умереть: the younger the infant at the time of vaccination, the higher was the rate of hospitalization and death.

4) Есть **прямая связь между дозой вакцины за раз** и риском попасть в больницу и умереть: The study reported a linear relationship between the number of vaccine doses administered at one time and the rate of hospitalization and death.

## Пневмококковая инфекция

Сколько длится постпрививочный иммунитет? Иммунитет к пневмококку вырабатывается сразу же после инъекции, примерно за 10–15 дней, и длится как минимум 3–5 лет, а иногда и дольше (около 8 лет). Повторная ревакцинация рекомендуется тем, кто имеет ослабленный иммунитет или входит в группу риска

[Источник](http://mama.neolove.ru/immunizations/spisok_privivok/vaktsinatsija_protiv_pnevmokokkovoj_infektsii.html)

## 5-валентная вакцина

### Дифтерия

Редко попадают в больницу. Даже из тех кто попадают летальный исход только в 3%

### Столбняк

### Полиомелит

### Коклюш

### Гемофильная инфекция



## 3-валентная

### Measles - Корь

По оценкам ВОЗ, в мире в 2011 году от кори умерло 158 тысяч человек, большинство из которых дети в возрасте до пяти лет. У 10% детей с недостаточностью питания и при отсутствии надлежащей медицинской помощи заболевание заканчивается смертельным исходом.[[1\]](https://ru.wikipedia.org/wiki/%D0%9A%D0%BE%D1%80%D1%8C#cite_note-WHO-fs286-1). В России заболеваемость корью на 2015 год составляет 3,2 случая на 100 000 человек [[2\]](https://ru.wikipedia.org/wiki/%D0%9A%D0%BE%D1%80%D1%8C#cite_note-2).

[https://ru.wikipedia.org/wiki/Корь](https://ru.wikipedia.org/wiki/%D0%9A%D0%BE%D1%80%D1%8C)



Measles affects about 20 million people a year,[3] primarily in the developing areas of Africa and Asia.[6] No other vaccine-preventable disease causes as many deaths.[12] In 1980, 2.6 million people died of it,[6] and in 1990, 545,000 died; by 2014, global vaccination programs had reduced the number of deaths from measles to 73,000.[8][13] The risk of death among those infected is usually 0.2%,[10] but may be up to 10% in people with malnutrition.[6] Most of those who die from the infection are less than five years old.[6] Measles is not believed to affect other animals.[6] Before immunization in the United States, between three and four million cases occurred each year.[10] As a result of widespread vaccination, the disease was eliminated from the Americas by 2016.[14]

...

Between 1987 and 2000, the case fatality rate across the [United States](https://en.wikipedia.org/wiki/United_States) was three measles-attributable deaths per 1000 cases, or 0.3%.[[26\]](https://en.wikipedia.org/wiki/Measles#cite_note-The_Clinical_Significance_of_Measles:_A_Review-26) In [underdeveloped nations](https://en.wikipedia.org/wiki/Developing_country) with high rates of [malnutrition](https://en.wikipedia.org/wiki/Malnutrition) and poor [healthcare](https://en.wikipedia.org/wiki/Healthcare), fatality rates have been as high as 28%.[[26\]](https://en.wikipedia.org/wiki/Measles#cite_note-The_Clinical_Significance_of_Measles:_A_Review-26) In [immunocompromised](https://en.wikipedia.org/wiki/Immunodeficiency) persons (e.g., people with [AIDS](https://en.wikipedia.org/wiki/AIDS)) the fatality rate is approximately 30%.[[27\]](https://en.wikipedia.org/wiki/Measles#cite_note-Sension1988-27) Risk factors for severe measles and its complications include [malnutrition](https://en.wikipedia.org/wiki/Malnutrition),[[28\]](https://en.wikipedia.org/wiki/Measles#cite_note-medscape-28) underlying immunodeficiency,[[28\]](https://en.wikipedia.org/wiki/Measles#cite_note-medscape-28) [pregnancy](https://en.wikipedia.org/wiki/Pregnancy),[[28\]](https://en.wikipedia.org/wiki/Measles#cite_note-medscape-28) and [vitamin A deficiency](https://en.wikipedia.org/wiki/Vitamin_A_deficiency).[[28\]](https://en.wikipedia.org/wiki/Measles#cite_note-medscape-28)[[29\]](https://en.wikipedia.org/wiki/Measles#cite_note-29) Even in previously healthy children, measles can cause serious illness requiring hospitalization.[[30\]](https://en.wikipedia.org/wiki/Measles#cite_note-ReferenceA-30) One out of every 1,000 measles cases will develop acute encephalitis, which often results in permanent brain damage.[[30\]](https://en.wikipedia.org/wiki/Measles#cite_note-ReferenceA-30) One or two out of every 1,000 children who become infected with measles will die from respiratory and neurologic complications.[[25\]](https://en.wikipedia.org/wiki/Measles#cite_note-cdc.gov-25)

[https://en.wikipedia.org/wiki/Measles](https://en.wikipedia.org/wiki/Measles)



### Rubella - Краснуха

Rubella is a contagious, **generally mild viral infection** that occurs most often in children and young adults.

While rubella virus infection usually causes a mild fever and rash illness in children and adults, infection during pregnancy, especially during the first trimester, can result in miscarriage, fetal death, stillbirth, or infants with congenital malformations, known as congenital rubella syndrome (CRS)

[http://www.who.int/mediacentre/factsheets/fs367/en/](http://www.who.int/mediacentre/factsheets/fs367/en/)

### Mumps/Parotitis - Свинка

The disease most commonly appears in people aged 40–60 years, but it may affect small children

[https://en.wikipedia.org/wiki/Parotitis](https://en.wikipedia.org/wiki/Parotitis)



Mumps is generally a mild childhood disease, most often affecting children between five and nine years old. However, the mumps virus can infect adults as well and when it does, possible complications are more likely to be serious. Complications of mumps can include meningitis (in up to 15% of cases), orchitis and deafness. Very rarely, mumps can cause encephalitis and permanent neurological damage.

[http://www.who.int/immunization/diseases/mumps/en/](http://www.who.int/immunization/diseases/mumps/en/)

## Other links

Court cases. Compensations.

https://www.historyofvaccines.org/content/articles/vaccine-injury-compensation-programs

[Vaccine Injury Compensation Data](https://www.hrsa.gov/vaccine-compensation/data/index.html)



The history of vaccination is more complicated than most people understand.  The anti-vaccine movement is hundreds of years old.

[http://drsuzanne.net/dr-suzanne-humphries-vaccines-vaccination](http://drsuzanne.net/dr-suzanne-humphries-vaccines-vaccination)



Dr. Kurt: why I will never choose to vaccinate my own son and any future kids my wife and I have

[https://www.naturalnews.com/055750_vaccines_flawed_research_medical_history.html](https://www.naturalnews.com/055750_vaccines_flawed_research_medical_history.html)



Death reports (confidential document leaked into the net)

[https://www.dropbox.com/s/20kl9i7feazw9bp/vaccin-death-reports.pdf?dl=0](https://www.dropbox.com/s/20kl9i7feazw9bp/vaccin-death-reports.pdf?dl=0)



------

Link to this document:

[https://gitlab.com/slukin/vaccines/blob/master/README.md](https://gitlab.com/slukin/vaccines/blob/master/README.md)
